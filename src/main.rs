use serde::Deserialize;
use serde_yaml;
use std::collections::BTreeMap;
#[derive(Deserialize)]
struct Setting {
    title: String,
    hola: usize,
    description: String,
}
fn main() {
    let mut options = comrak::Options::default();
    options.extension.table = true;

    let settings: BTreeMap<String, Setting> =
        serde_yaml::from_str(include_str!("../data.yaml")).unwrap();
    for (_, setting) in &settings {
        println!("{}", setting.title);
        println!("===\n{}===", setting.description);
        let asbytes: Vec<String> = setting
            .description
            .clone()
            .as_bytes()
            .iter()
            .map(|byte| format!("{:02X}", *byte))
            .collect();
        println!("AsBytes: {}\n", asbytes.join(":"));
        println!(
            "Markdown2HTML:\n{}\n",
            comrak::markdown_to_html(&setting.description, &options)
        );
    }
}
